/**
 * Created by eralp on 21/06/2017.
 */


export namespace mytypes {

  export class Person {
    constructor(public name: string, public lastname: string) {

    }

    static GetAllPeople(): Person[] {
      let allPeople: Person[] = [
        new Person("Ali", "Veli"),
        new Person("Deniz", "Akçe")
      ]
      return allPeople;
    }

  }

}
