import {Component} from '@angular/core';
import {mytypes} from "./mytypelist";
import Person = mytypes.Person;
import {FormsModule} from "@angular/forms";

@Component({
  selector: 'app-root',
  template: `
    <div *ngIf="this.people.length!=0">
      <h1>Person List</h1>
      <!-- <div *ngFor="let f of people">{{f.name}}</div> -->
      <ul>
        <li *ngFor="let f of people" (click)="ShowMyDetail(f)"> {{f.name}}</li>
      </ul>
    </div>

    <input [(ngModel)]="title">
    
    <span>{{title}}</span>

    <input [(ngModel)]="title" [value]="title">
    
    
  `
})
export class AppComponent {
  title = 'app';
  people;

  constructor() {
    this.people = mytypes.Person.GetAllPeople();
  }

  ShowMyDetail(item: Person) {
    this.title = item.name;
    console.log(item.name + "" + item.lastname)
  }
}

