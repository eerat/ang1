import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { SimpleComponent } from './simple/simple.component';
import { MypipePipe } from './mypipe.pipe';
import {FormsModule} from '@angular/forms'

@NgModule({
  declarations: [
    AppComponent,
    SimpleComponent,
    MypipePipe
  ],
  imports: [
    BrowserModule,FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
