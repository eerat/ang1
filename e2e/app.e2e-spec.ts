import { Temp1Page } from './app.po';

describe('temp1 App', () => {
  let page: Temp1Page;

  beforeEach(() => {
    page = new Temp1Page();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
